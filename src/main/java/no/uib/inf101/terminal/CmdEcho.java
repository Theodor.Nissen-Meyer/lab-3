package no.uib.inf101.terminal;

public class CmdEcho implements Command{

    @Override
    public String run(String[] args) {
        // TODO Auto-generated method stub
        String res = "";
        for (String string : args) {
            res += string + " ";
        }
        return res;
    }

    @Override
    public String getName() {
        // TODO Auto-generated method stub
        return "echo";
    }


}
